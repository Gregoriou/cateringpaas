
jQuery.easing['jswing']=jQuery.easing['swing'];jQuery.extend(jQuery.easing,{easeOutBounce:function(x,t,b,c,d){if((t/=d)<(1/2.75)){return c*(7.5625*t*t)+b}else if(t<(2/2.75)){return c*(7.5625*(t-=(1.5/2.75))*t+.75)+b}else if(t<(2.5/2.75)){return c*(7.5625*(t-=(2.25/2.75))*t+.9375)+b}else{return c*(7.5625*(t-=(2.625/2.75))*t+.984375)+b}}});

$('.vertmove').hover(function() {
    $(this).stop().animate({
        top: -50
    }, 900, "easeOutBounce");
}, function() {
    $(this).stop().animate({
        top: 0
    }, 900, "easeOutBounce");
});
function getQueryVariable(variable)
{
       var query = window.location.href;
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}

$(document).ready(function(){

  $("#addImageBtn").click(function(){
    var html = '<tr> <td><input id="num" value="7" type="number"></td><td><input type="text" placeholder="Name of your image"></td><td><input type="text" placeholder="Your link"></td></tr>';
jQuery("#images-json-table > tbody:last").after(html);
});
});
function imagesToJSON(table){//begin function

//array to hold the key name
var keyName;

//array to store the keyNames for the objects
var keyNames = [];

//array to store the objects
var objectArray = [];

//get the number of cols
var numOfCols = table.rows[0].cells.length;

//get the number of rows
var numOfRows = table.rows.length;

//add the opening [ array bracket
objectArray.push('"Images":')
objectArray.push("[");

//loop through and get the propertyNames or keyNames
for(var i = 0; i < numOfCols; i++){//begin for loop
  //store the html of the table heading in the keyName variable
keyName = table.rows[0].cells[i].innerHTML;

  //add the keyName to the keyNames array
  keyNames.push(keyName);
}//end for loop

  //loop through rows
  for(var i = 1; i < numOfRows; i++){//begin outer for loop

    //add the opening { object bracket
    objectArray.push("{\n");

  for(var j=0; j < numOfCols; j++){//begin inner for loop

 //extract the text from the input value in the table cell
 var inputValue = table.rows[i].cells[j].children[0].value;

  //store the object keyNames and its values
 objectArray.push("\"" + keyNames[j] + "\":" + "\"" + inputValue + "\"");

//if j less than the number of columns - 1(<-- accounting for 0 based arrays)
if(j < (numOfCols - 1)){//begin if then

  //add the , seperator
  objectArray.push(",\n");

}//end if then

  }//end inner for loop

    //if i less than the number of rows - 1(<-- accounting for 0 based arrays)
    if(i < (numOfRows - 1)){//begin if then

      //add the closing } object bracket followed by a , separator
      objectArray.push("\n},\n");

  }
    else{

      //add the closing } object bracket
      objectArray.push("\n}");

    }//end if then else

  }//end outer for loop

   //add the closing ] array bracket
   objectArray.push("],");

  return objectArray.join("");

}//end function

$("#images-test-form").on("click","#imagesSubmit",function(e){

  //stop form form submitting
  e.preventDefault();

  //the table object
  var table = $("#images-json-table")[0];

  //display the results
$("#imagesResults").val(imagesToJSON(table));

});

function reviewsToJSON(table){//begin function

//array to hold the key name
var keyName;

//array to store the keyNames for the objects
var keyNames = [];

//array to store the objects
var objectArray = [];

//get the number of cols
var numOfCols = table.rows[0].cells.length;

//get the number of rows
var numOfRows = table.rows.length;

//add the opening [ array bracket
objectArray.push('"Reviews":')
objectArray.push("[");

//loop through and get the propertyNames or keyNames
for(var i = 0; i < numOfCols; i++){//begin for loop
  //store the html of the table heading in the keyName variable
keyName = table.rows[0].cells[i].innerHTML;

  //add the keyName to the keyNames array
  keyNames.push(keyName);
}//end for loop

  //loop through rows
  for(var i = 1; i < numOfRows; i++){//begin outer for loop

    //add the opening { object bracket
    objectArray.push("{\n");

  for(var j=0; j < numOfCols; j++){//begin inner for loop

 //extract the text from the input value in the table cell
 var inputValue = table.rows[i].cells[j].children[0].value;

  //store the object keyNames and its values
 objectArray.push("\"" + keyNames[j] + "\":" + "\"" + inputValue + "\"");

//if j less than the number of columns - 1(<-- accounting for 0 based arrays)
if(j < (numOfCols - 1)){//begin if then

  //add the , seperator
  objectArray.push(",\n");

}//end if then

  }//end inner for loop

    //if i less than the number of rows - 1(<-- accounting for 0 based arrays)
    if(i < (numOfRows - 1)){//begin if then

      //add the closing } object bracket followed by a , separator
      objectArray.push("\n},\n");

  }
    else{

      //add the closing } object bracket
      objectArray.push("\n}");

    }//end if then else

  }//end outer for loop

   //add the closing ] array bracket
   objectArray.push("],");

  return objectArray.join("");

}//end function

/*
Function responsible for extracting the returned token passed back through
the browser location in the form https://caasbusiness.com/#id_token=id_token
The function should only return the id_tokens value. This will be used for the
Authentication
*/


/*End of IMAGES JS*/
$(document).ready(function(){
  $("#addMenuBtn").click(function(){
var html = '<tr> <td> <input type="text" placeholder="Steak 21oz"></td> <td> <input type="text" placeholder="Our legendary steak served with..."> </td> <td> <input type="number" placeholder="22.99" pattern="[0-9]+(\\.[0-9][0-9]?)?"> </td> <td> <input type="text" placeholder="Image link to your steak"> </td> </tr>';
jQuery("#menu-json-table > tbody:last").after(html);
});
});

function menuToJSON(table){//begin function

//array to hold the key name
var keyName;

//array to store the keyNames for the objects
var keyNames = [];

//array to store the objects
var objectArray = [];

//get the number of cols
var numOfCols = table.rows[0].cells.length;

//get the number of rows
var numOfRows = table.rows.length;

//add the opening [ array bracket
objectArray.push('"Menu_Items":')
objectArray.push("[");

//loop through and get the propertyNames or keyNames
for(var i = 0; i < numOfCols; i++){//begin for loop

  //store the html of the table heading in the keyName variable
keyName = table.rows[0].cells[i].innerHTML;

  //add the keyName to the keyNames array
  keyNames.push(keyName);

}//end for loop

  //loop through rows
  for(var i = 1; i < numOfRows; i++){//begin outer for loop

    //add the opening { object bracket
    objectArray.push("{\n");

  for(var j=0; j < numOfCols; j++){//begin inner for loop

 //extract the text from the input value in the table cell
 var inputValue = table.rows[i].cells[j].children[0].value;

  //store the object keyNames and its values
 objectArray.push("\"" + keyNames[j] + "\":" + "\"" + inputValue + "\"");

//if j less than the number of columns - 1(<-- accounting for 0 based arrays)
if(j < (numOfCols - 1)){//begin if then

  //add the , seperator
  objectArray.push(",\n");

}//end if then

  }//end inner for loop

    //if i less than the number of rows - 1(<-- accounting for 0 based arrays)
    if(i < (numOfRows - 1)){//begin if then

      //add the closing } object bracket followed by a , separator
      objectArray.push("\n},\n");

  }
    else{

      //add the closing } object bracket
      objectArray.push("\n}");

    }//end if then else

  }//end outer for loop

   //add the closing ] array bracket
   objectArray.push("],");

  return objectArray.join("");

}//end function


$("#menu-test-form").on("click","#menuSubmit",function(e){

  //stop form form submitting
  e.preventDefault();

  //the table object
  var table = $("#menu-json-table")[0];

  //display the results
$("#menuResults").val(menuToJSON(table));

});


$(document).ready(function(){
  $("#addRowBtn").click(function(){
var html = '<tr><td><select id="daySelect"><option value="Monday">Monday</option><option value="Tuesday">Tuesday</option><option selected value="Wednesday">Wednesday</option><option value="Thursday">Thursday</option><option value="Friday">Friday</option><option value="Saturday">Saturday</option><option value="Sunday">Sunday</option><select></td><td><select id="daySelect"><option value=0.00>0:00</option><option value=1.00>1:00</option><option value="2.00">2:00</option><option value="3.00">3:00</option><option value=4.00>4:00</option><option value=5.00>5:00</option><option value=6.00>6:00</option><option value=8.00>8:00</option><option selected value=9.00>9:00</option><option value=10.00>10:00</option><option value=11.00>11:00</option><option value=12.00>12:00</option><option value=13.00>13:00</option><option value=14.00>14:00</option><option value=15.00>15:00</option><option value=16.00>16:00</option><option value=17.00>17:00</option><option value=18.00>18:00</option><option value=19.00>19:00</option><option value=20.00>20:00</option><option value=21.00>21:00</option><option value=22.00>22:00</option><option value=23.00>23:00</option><select> </td><td><select id="daySelect"><option value=0.00>0:00</option><option value=1.00>1:00</option><option value="2.00">2:00</option><option value="3.00">3:00</option><option value=4.00>4:00</option><option value=5.00>5:00</option><option value=6.00>6:00</option><option value=8.00>8:00</option><option value=9.00>9:00</option><option value=10.00>10:00</option><option value=11.00>11:00</option><option value=12.00>12:00</option><option value=13.00>13:00</option><option value=14.00>14:00</option><option value=15.00>15:00</option><option value=16.00>16:00</option><option selected value=17.00>17:00</option><option value=18.00>18:00</option><option value=19.00>19:00</option><option value=20.00>20:00</option><option value=21.00>21:00</option><option value=22.00>22:00</option><option value=23.00>23:00</option></td></tr>';
jQuery("#json-table > tbody:last").after(html);
});
});

function timeToJSON(table){//begin function

//array to hold the key name
var keyName;

//array to store the keyNames for the objects
var keyNames = [];

//array to store the objects
var objectArray = [];
//get the number of cols
var numOfCols = table.rows[0].cells.length;

//get the number of rows

var numOfRows = table.rows.length;

//add the opening [ array bracket
objectArray.push('"Open_Hours":')
objectArray.push("[");

//loop through and get the propertyNames or keyNames
for(var i = 0; i < numOfCols; i++){//begin for loop

  //store the html of the table heading in the keyName variable
keyName = table.rows[0].cells[i].innerHTML;

//replacing the spaces in the html rendered format of the table columns
var keyName=keyName.replace(' ', '_');
  //add the keyName to the keyNames array
  keyNames.push(keyName);

}//end for loop

  //loop through rows
  for(var i = 1; i < numOfRows; i++){//begin outer for loop

    //add the opening { object bracket
    objectArray.push("{\n");

  for(var j=0; j < numOfCols; j++){//begin inner for loop

 //extract the text from the input value in the table cell
 var inputValue = table.rows[i].cells[j].children[0].value;

  //store the object keyNames and its values
 objectArray.push("\"" + keyNames[j] + "\":" + "\"" + inputValue + "\"");

//if j less than the number of columns - 1(<-- accounting for 0 based arrays)
if(j < (numOfCols - 1)){//begin if then
  //add the , seperator
  objectArray.push(",\n");

}//end if then

  }//end inner for loop

    //if i less than the number of rows - 1(<-- accounting for 0 based arrays)
    if(i < (numOfRows - 1)){//begin if then

      //add the closing } object bracket followed by a , separator
      objectArray.push("\n},\n");

  }
    else{
      //add the closing } object bracket
      objectArray.push("\n}");

    }//end if then else

  }//end outer for loop

   //add the closing ] array bracket
   objectArray.push("],");

  return objectArray.join("");
}//end function

$("#test-form").on("click","#hoursSubmit",function(e){

  //stop form form submitting
  e.preventDefault();

  //the table object
  var table = $("#json-table")[0];

  //display the results
$("#results").val(timeToJSON(table));

});


/*THE START OF THE COLLECTION FOR CHEF NAMES*/
$(document).ready(function(){
  $("#addChefBtn").click(function(){
var html = '<tr><td><input type=text> </td> <td> <input type=text></td> <td> <input type=text></td></tr>';
jQuery("#chef-json-table > tbody:last").after(html);
});
});

function formToJSON(table){//begin function

//array to hold the key name
var keyName;

//array to store the keyNames for the objects
var keyNames = [];

//array to store the objects
var objectArray = [];

//get the number of cols
var numOfCols = table.rows[0].cells.length;

//get the number of rows
var numOfRows = table.rows.length;

//add the opening [ array bracket
  objectArray.push('"Chefs":')
  objectArray.push("[");
//loop through and get the propertyNames or keyNames
for(var i = 0; i < numOfCols; i++){//begin for loop

  //store the html of the table heading in the keyName variable
keyName = table.rows[0].cells[i].innerHTML;

  //add the keyName to the keyNames array
  keyNames.push(keyName);

}//end for loop


  //loop through rows
  for(var i = 1; i < numOfRows; i++){//begin outer for loop

    //add the opening { object bracket
    objectArray.push("{\n");

  for(var j=0; j < numOfCols; j++){//begin inner for loop

 //extract the text from the input value in the table cell
 var inputValue = table.rows[i].cells[j].children[0].value;

  //store the object keyNames and its values
 objectArray.push("\"" + keyNames[j] + "\":" + "\"" + inputValue + "\"");

//if j less than the number of columns - 1(<-- accounting for 0 based arrays)
if(j < (numOfCols - 1)){//begin if then

  //add the , seperator
  objectArray.push(",\n");

}//end if then

  }//end inner for loop

    //if i less than the number of rows - 1(<-- accounting for 0 based arrays)
    if(i < (numOfRows - 1)){//begin if then

      //add the closing } object bracket followed by a , separator
      objectArray.push("\n},\n");
  }
    else{

      //add the closing } object bracket
      objectArray.push("\n}");

    }//end if then else

  }//end outer for loop

   //add the closing ] array bracket
   objectArray.push("]}");
  return objectArray.join("");
}//end function


$("#chef-form").on("click","#chefSubmit",function(e){

  //stop form form submitting
  e.preventDefault();

  //the table object
  var table = $("#chef-json-table")[0];

  //display the results
$("#resultsChefs").val(formToJSON(table));

});

//This is for the for showing the elements in the modal depending on the theme choice radio button
var FormStuff = {

  init: function() {
    this.applyConditionalRequired();
    this.bindUIActions();
  },

  bindUIActions: function() {
    $("input[type='radio'], input[type='checkbox']").on("change", this.applyConditionalRequired);
  },

  applyConditionalRequired: function() {

    $(".require-if-active").each(function() {
      var el = $(this);
      if ($(el.data("require-pair")).is(":checked")) {
        el.prop("required", true);
      } else {
        el.prop("required", false);
      }
    });
  }
};

FormStuff.init();

function toJSONString( form ) {
        var obj = {};
        var elements = form.querySelectorAll( "input, select, textarea" );
        for( var i = 0; i < elements.length; ++i ) {
            var element = elements[i];
            var name = element.name;
            var value = element.value;

        if( name ) {
                obj[ name ] = value;
            }
        }
        return JSON.stringify( obj );
    }
    function toFormValue( string ) {
        var obj = JSON.parse(string);

        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                var content = obj[key];
                var input = document.getElementsByName(key)[0];
                input.value = content;
            }
        }
    }
    var json = {};
    var form = document.getElementById( "form" );
    form.addEventListener( "submit", function( e ) {
      e.preventDefault();
      var r = toJSONString( this );
      console.log("var r is:"+r);
      var radioValue = $("input[name='Theme_Choice']:checked").val();
                 if(radioValue =="1"){
                     alert("Picked theme  - " + radioValue);
                     function toJSONString2( form ) {
                             var obj = {};
                             var elements = form.querySelectorAll(".theme1");
                             for( var i = 0; i < elements.length; ++i ) {
                                 var element = elements[i];
                                 var name = element.name;
                                 var value = element.value;

                             if( name ) {
                                     obj[ name ] = value;
                                 }
                             }
                             return JSON.stringify( obj );
                         }
                         function toFormValue( string ) {
                             var obj = JSON.parse(string);

                             for (var key in obj) {
                                 if (obj.hasOwnProperty(key)) {
                                     var content = obj[key];
                                     var input = document.getElementsByName(key)[0];
                                     input.value = content;
                                 }
                             }
                         }
                      var theme1 = toJSONString2( this );
                      var stringEdit =String(theme1).slice(1);
                      var restName = $("input[name='Restaurant'").val();
                      var email = $("input[name='Email'").val();
                      var themeField=('"Theme_Choice":"1  ",');
                      var addString = '{"Restaurant":"'+restName + '",'+themeField+'"Email":'+'"'+email+'",';
                      console.log(addString);
                      stringEdit=addString+stringEdit;
                      console.log("The stringEdit is: "+stringEdit);
                      console.log("The restaurant name is:"+restName);

                      //removes last character in the object
                      var r=String(stringEdit).slice(0, -1);
                      r+=',';
                      r+="\n";
                        console.log("sliced r: "+r);
                            var gallery=imagesToJSON($("#images1-json-table")[0]);
                            console.log("im ages array is: "+gallery);
                            r+=gallery;
                            console.log("after adding gallery"+r);
                            var r=String(r).slice(0, -1);
                            r+='}';
                 }
                 else if (radioValue =="2"){
                 alert  ("Picked theme 2 - "+ radioValue);
                 var r=String(r).slice(0, -1);
                 r+=',';
                 r+="\n";
                   console.log("sliced r: "+r);
                   var reviews = reviewsToJSON($("#reviews-json-table")[0]);
                   console.log("Reviews:"+reviews);
                   r+=reviews;
                       var time=timeToJSON($("#json-table")[0]);
                       console.log(time);
                     r+=time;
                     console.log("1st iter: "+r);

                       var images=imagesToJSON($("#images-json-table")[0]);
                       console.log("images array is: "+images);
                       r+=images;
                       console.log("2nd iter: "+r);
                       var menu=menuToJSON($("#menu-json-table")[0]);
                         console.log("time array is: "+menu);
                       r+=menu;
                       console.log("3rd iter: "+r);
                       var chefs=formToJSON($("#chef-json-table")[0]);
                       console.log("chef array is: "+ chefs);
                       r+=chefs;
                       console.log("4th iter: "+r);
                 }

//adding the json array for the time
//if ($("#form input[value='2']").is(':checked')){
//removes last character in the object


      json = r;
      console.log("Json payload for AJAX:"+json);
      var str = window.location.href;
      console.log("window location is: "+ str);
      var res = str.split("=");
      console.log("following split: "+res);
      var r = res[1];
      r=r.split("&");
      var id_token = r[0];
      console.log(id_token);

      var myHeaders = new Headers();
      myHeaders.set('id_token',id_token);
    //  console.log(myHeaders.get('id_token'));
      /*if (myHeaders.get('id_token')==null){
     eyJraWQiOiJSRUVxS1UxU2ZvZ3hROVZ3eDBKNkRJS2hndnNHYjdNeFB6eWFrUkZLZzJZPSIsImFsZyI6IlJTMjU2In0.eyJhdF9oYXNoIjoiR0R3N3VjWXhDNmF1SUJ0NjczeFhUQSIsInN1YiI6IjU5MmU4MWEyLWUzNjctNDA5NC1hMDRhLWFhMzYyMzk3MmQzYiIsImF1ZCI6IjNnMzI1dmIza2x1bDdybWduYjRmbHAxNDMiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE1NTYzODUwOTgsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTIuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0yX2RSMGRoWEpZVSIsIm5hbWUiOiJHcmVnIEciLCJjb2duaXRvOnVzZXJuYW1lIjoiNTkyZTgxYTItZTM2Ny00MDk0LWEwNGEtYWEzNjIzOTcyZDNiIiwiZXhwIjoxNTU2Mzg4Njk4LCJpYXQiOjE1NTYzODUwOTgsImVtYWlsIjoiZ3JlZy1nMTdAaG90bWFpbC5jby51ayJ9.VTEVGpomf4Eq_Bp72pTSghRlHSD53H9eKCrF56BkXZs7UNBCkv3p6Qd-b4evKgOIMz084rELRtMwH5E_TaO30mH38x_6OK8JsPdH7AfaJ8uGv1MpfhdRD3msxiFNFCwyVB6cmDUM9lyYGE-NiR2JOjTF1etMxC1ct2PJpQmFbhsxI0ZcveYEP0FvC4ccnm_GgeP82Zf5J_a3KTurKGgP4r7t9gQ5i-fvw3C0WGnd4lsE8KgRT1nGZS1tbuNKD2z4EZBPqoDD-a_UtqvOjDI0Uy8G_RXsbP1r-EXuwaEqXuJxCCrkmZOt6ViBt0-h-g8NfrV__N1dgzmL8TpYRzDVwQ');
}*/
console.log("the id_token is: "+myHeaders.get('id_token'));
var id=myHeaders.get('id_token');
      //display the results
      //change url to updated one, https://hpat54w6ob.ex  ecute-api.eu-west-2.amazonaws.com/test and pass in authorisation through the current header (find way to get the current header and send this information in the headers section)
    var url = "https://hpat54w6ob.execute-api.eu-west-2.amazonaws.com/CaaS-Restaurant";
    $.ajax({
           type: "POST",
           crossDomain: true,
           url: url,
           data: json,
       success: function(data) { alert('data: ' + data); },
       contentType: "application/json",
//changed to jsonP and application/json
       dataType: 'json',
       headers: {
         'Authorization': id_token,
         //eyJraWQiOiJSRUVxS1UxU2ZvZ3hROVZ3eDBKNkRJS2hndnNHYjdNeFB6eWFrUkZLZzJZPSIsImFsZyI6IlJTMjU2In0.eyJhdF9oYXNoIjoicEI4NlBzOU1VdXZaVENtR01DRjl1dyIsInN1YiI6IjQyZTFhYzA4LWQ4N2EtNDU1YS04ODE2LWVlMDVlNjQ4YWZjMiIsImF1ZCI6IjNnMzI1dmIza2x1bDdybWduYjRmbHAxNDMiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTU2MTE5Mjc3LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtd2VzdC0yLmFtYXpvbmF3cy5jb21cL2V1LXdlc3QtMl9kUjBkaFhKWVUiLCJuYW1lIjoiZ3JlZyB0aGUgZWdnIiwiY29nbml0bzp1c2VybmFtZSI6IjQyZTFhYzA4LWQ4N2EtNDU1YS04ODE2LWVlMDVlNjQ4YWZjMiIsImV4cCI6MTU1NjEyMjg3NywiaWF0IjoxNTU2MTE5Mjc3LCJlbWFpbCI6ImdyZWctZzE3QGhvdG1haWwuY28udWsifQ.i-A67UVt4l63_elgLHnaLlfOROTgYu4X5r1NN75YDK87GnzeQCGsDVR0iVAjFlA6LjRmNKWWT4VOuFoyY4HbPjSMfqKTa--zrrpSILqeotvsvzrdcHafcCoS-cwi0U_GbQwGkjHTg0p3vXzXRnBmUM1StDC-iAjCzCxklL68pwVrDPxxd2m184JIJqK0ERAPNgqV670KxBzj_T0rjzuQEHZSpA8O6qgx53Gb3E9ciKS7CrtFAJtiJoEommfa0SYmaf5Bs7_D1ljnYCkDgApeeMHswNpwLJJ5-0iR7fiJp42_AsTyaiQBTlbseXu_m1L9iebVgCTe6ax2M7rlUrdcWQ

    }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
 });

/*$if(document.getElementsByName('themeChoice2').checked) {
$('#menu').remove();
}*/

//Greg Gregoriou: Code here was an adaptation from https://codepen.io/takaneichinose/pen/NEpRdy?page=1&
$(document).ready(function() {
  $('a[href^="#"]').on('click', function(evt) {
    evt.preventDefault();

    //nav in index.html is animated
    var link = $(this).attr('href');
    var less = (!$('#nav','#nav2').hasClass('active')) ? 50 : 0;

    $('html, body').animate({
        scrollTop: $(link).offset().top - less
    }, 750);
  });

  $(window).on('scroll', function() {
    if ($(this).scrollTop() > 100) {
      $('#nav').addClass('active');
      $('#nav2').addClass('active');
    }
    else {
      $('#nav').removeClass('active');
      $('#nav2').removeClass('active');

    }
  });
});
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("btn-exp")

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

// Get the modal
var modal2 = document.getElementById('myModal');

// Gets the second button that opens the modal
var btn2 = document.getElementById("btn-exp2")

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn2.onclick = function() {
  var url = "https://caasbusiness-api.auth.eu-west-2.amazoncognito.com/login?response_type=token&client_id=3g325vb3klul7rmgnb4flp143&redirect_uri=https://caasbusiness.com/account.html"
location.replace(url);
    modal2.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal2.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal2) {
        modal2.style.display = "none";
    }
}

$(document).ready(function(){
  //start
  var str = window.location.href;
  console.log("window location is: "+ str);
  var res = str.split("=");
  console.log("following split: "+res);
  var r = res[2];
  r=r.split("&");
  var id_token = r[0];
  console.log(id_token);

  var url = "https://caasbusiness.auth.eu-west-2.amazoncognito.com/oauth2/userInfo";
  $.ajax({
         type: "GET",
         crossDomain: true,
         url: url,
     success: function(data) { //alert('Welcome: '+ data.username + "\n"+data.email );
   $("#TextAWS").append(data.username+", \n"+ "Your email is: "+data.email);
   $("#logoutDiv").prepend(data.username);
  },
     contentType: "application/json",
  //changed to jsonP and application/json
     dataType: 'json',
     headers: {
       'Authorization': "Bearer "+id_token

  }
  });
  e.preventDefault(); // avoid to execute the actual submit of the form.
});
